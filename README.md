# GAZEBO simulation 
#### Purdue FALL 2018  ECET581 Project

## 1. Install
```bash
$ cd ~/catkin_ws/src/
$ git clone https://gitlab.com/stnoah1/ros.git
$ cd ~/catkin_ws && catkin_make
```
## 2. Run
### 2.1 Simulate in world - Turtlebot3 ME BUILDING
```bash
$ roslaunch turtlebot3_gazebo turtlebot3_me_building.launch
```
### 2.2 Drive Turtlebot - Teleoperation on Gazebo
```bash
$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch
```
### 2.3 Control Objects with Topic
>### 2.3.1 Carry Item
><h5> - This topic makes a specific object appear and disappear from the top of the robot.</h5>
>
>#### Publish topic
>```python
>pub = rospy.Publisher('carry_item', String, queue_size=10)
>```
>#### Massage Example:
>```json
>"{'item':'box','type':'appear','color':'blue'}"
>```
>#### Message Options:
>**`item`** : `box` `cylinder`<br/>
>**`type`** : `appear` `disappear`<br/>
>**`color`** : `red` `orange` `red` `blue` `black` `blue`
>#### Note
>Do not publish topic repeatidly! Publish topic only once in each time.
>#### Example Code:
>```python
>#!/usr/bin/env python
>
>import rospy
>import json
>
>from std_msgs.msg import String
>
>def test_topic():
>  rospy.init_node('test_topic')
>  pub = rospy.Publisher('carry_item', String, queue_size=10)
>  msg = {
>      'item':'box',     # box, cylinder
>      'type':'appear',  # appear, disappear
>      'color':'blue'    # red, orange, red, blue, black, blue
>  }
>  rate = rospy.Rate(10)
>  while(pub.get_num_connections() == 0):
>      rate.sleep()
>  pub.publish(str(msg))
>
>
>if __name__ == '__main__':
>  try:
>    test_topic()
>  except rospy.ROSInterruptException:
>    pass
>
>```
>### 2.3.2 Change State Light
><h5> - This topic changes the state light of the machines</h5>
>
>#### Publish topic
>```python
>pub = rospy.Publisher('change_light', String, queue_size=10)
>```
>#### Massage Example:
>```json
>"{'id':'paintMixer01, 'state':'on'}"
>```
>#### Message Options:
>**`id`** : `curingOven01` `paintMachine01` `paintMixer01` `paintMixer02` `charger01` `itemInv01` `paintInv01`<br/>
>**`state`** : `on` `off`<br/>
>#### Note
>Do not publish topic repeatidly! Publish topic only once in each time.
>#### Example Code:
>```python
>#!/usr/bin/env python
>
>import rospy
>import json
>
>from std_msgs.msg import String
>
>def test_topic():
>  rospy.init_node('test_change_light')
>  pub = rospy.Publisher('change_light', String, queue_size=10)
>  msg = {
>      'id':'paintMixer01', # curingOven01, paintMachine01, paintMixer01, paintMixer02, charger01, itemInv01, paintInv01
>      'state':'on'         # on, off
>  }
>  rate = rospy.Rate(10)
>  while(pub.get_num_connections() == 0):
>      rate.sleep()
>  pub.publish(str(msg))
>
>
>if __name__ == '__main__':
>  try:
>    test_topic()
>  except rospy.ROSInterruptException:
>    pass
>
>```
## 3. World Information
### 3.1 IOT POSE

| IOT id   |     Position(x,y)      |  Orientation(rad) |  topic | type|
|:----------|:-------------:|:------:|:----------:|:--------:|
| curingOven01 | 9.56102, 1.22989 | 2.10938 |dev07_dispense|object|
| paintMachine01 | 3.70945, -0.370969 | 2.30822 | dev06_dispense| object|
| paintMixer01 | 11.8436, -0.616907 | 0.52207| dev04_dispense| object|
| paintMixer02 | 14.7755, -5.84764 | 0.570093 | dev05_dispense | object|
| charger01 | -5.21089, -4.0842 | -1.66984 |dev01_charge|time|
| itemInv01 | 12.7427, -7.05561 | -1.72853 | dev02_dispenseItems|value|
| paintInv01 | 3.07728, -5.04366 | -1.77961 |dev03_dispenseCans|value|
| storage01 | -12.3276, -3.39296 | -1.73613  |

### 3.1 IOT TOPIC
|NO| IOT id   |  topic | type|
|:---|:----------|:----------:|:--------:|
|1| curingOven01 |dev07_dispense|object|
|2| curingOven01 |dev07_curePaint|time|
|3| paintMachine01 | dev06_dispense| object|
|4| paintMachine01 | dev06_paint| time|
|5| paintMixer01 | dev04_dispense| object|
|6| paintMixer01 | dev04_mixPaint| time|
|7| paintMixer02 | dev05_dispense | object|
|8| paintMixer02 | dev05_mixPaint | time|
|9| charger01 |dev01_charge|time|
|10| itemInv01 | dev02_dispenseItems|value|
|11| paintInv01|dev03_dispenseCans|value|



## 4. Useful Links
### 4.1 CAD models
#### Website
* [3D Warehouse](https://3dwarehouse.sketchup.com/search/?q=machine&searchTab=model)
* [3DGEMS](http://data.nvision2.eecs.yorku.ca/3DGEMS/)

#### Machines
* [Mixer(M)](https://3dwarehouse.sketchup.com/model/8b4bfe4e58005ddac5461375dfe222d8/water-machine)
* [Painting machine(L)](https://3dwarehouse.sketchup.com/model/209987bd79868ef7bc60ba68efddd93a/Machine)
* [Curing machine(L)](https://3dwarehouse.sketchup.com/model/96650332270e902468b117e3280e39be/Machine)
* [Inventory, Storage(M)](https://3dwarehouse.sketchup.com/model/uf8490ad7-cef6-4a6a-b43f-0ff56c1409dd/shelving)
* [Charging station(S)](https://3dwarehouse.sketchup.com/model/bed9a007d61d8564d1861252313864bd/Chargepoint-ct3000-Car-Charger)

#### ETC
* [Traffic Light](https://3dwarehouse.sketchup.com/model/7457673f0f7e6066158dc5cdab60bc04/Traffic-Light)

### 4.2 Control light
* http://answers.gazebosim.org/question/13930/basic-in-environmentworld-control/
* http://gazebosim.org/tutorials?tut=led_plugin&cat=plugins