#!/usr/bin/env python

import rospy
import time
from vipo_msgs.msg import VipoMsg
from test_item import test_topic
from test_change_light import change_light


TOPIC_INFO ={
    "iot_id":"paintMixer01",
    "item_msg": "{'item':'cylinder','type':'appear','color':'orange'}"
}


def carry_item(data):
    test_topic(TOPIC_INFO['item_msg'])
    change_light({'id':TOPIC_INFO['iot_id'],'state':'on'})

if __name__ == "__main__":
    rospy.init_node('dev04_dispense')
    rospy.Subscriber("dev04_dispense", VipoMsg, carry_item)
    rospy.spin()
    
