#!/usr/bin/env python
# license removed for brevity
import rospy
import math
from vipo_msgs.msg import VipoMsg

# Inv: [3.2, -5.8], PaintingMachine: [3.0, 0.35], ItemInv: [14, -8]
# CurMachine: [9.5, 2], MixerB: [12, 0], MixerA: [15, -5]
# Storage: [-12, -4]

def talker():
    pub = rospy.Publisher('waypoint', VipoMsg, queue_size=10)
    rospy.init_node('vipo_msg_publisher', anonymous=True)
    rate = rospy.Rate(10)
    msg = VipoMsg()

    msg.location = [11.85, -0.0616, 0.522]  # paint mixer 01
    # msg.location = [3.7, -0.37, 2.3]  # paint machine
    # msg.location = [9.56, 1.22, 2.1]  # curing oven
    # msg.location = [14.77, -5.8, 0.57]  # paint mixer 02
    # msg.location = [-5.2, -4.1, -1.66]  # charger
    # msg.location = [12.7, -7.1, -1.7]  # paint inv
    # msg.location = [3.1, -5, -1.78]  # item inv
    # msg.location = [-12.4, -3.4, -1.7]  # storage

    while not rospy.is_shutdown():
        # print(type(msg), msg)
        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
