#!/usr/bin/env python

import rospy

from std_msgs.msg import String

def test_topic():
  rospy.init_node('test_msg_topic')
  pub = rospy.Publisher('dev03_dispenseCans', String, queue_size=10)

  rate = rospy.Rate(10)
  while(pub.get_num_connections() == 0):
      rate.sleep()
  pub.publish('test')


if __name__ == '__main__':
  try:
    test_topic()
  except rospy.ROSInterruptException:
    pass
