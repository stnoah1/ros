#!/usr/bin/env python

import rospy
import tf
from gazebo_msgs.srv import DeleteModel, SpawnModel, GetModelState, SetModelState
from gazebo_msgs.msg import ModelState
from geometry_msgs.msg import Pose, Point, Quaternion
import time

class RosService():
  def __init__(self):
    rospy.wait_for_service("gazebo/spawn_sdf_model")
    rospy.wait_for_service('gazebo/delete_model')
    rospy.wait_for_service('gazebo/get_model_state')
    rospy.wait_for_service('gazebo/set_model_state')
    self.spawn_model_prox = rospy.ServiceProxy("gazebo/spawn_sdf_model", SpawnModel)
    self.del_model_prox = rospy.ServiceProxy('gazebo/delete_model', DeleteModel)
    self.set_model_prox = rospy.ServiceProxy('gazebo/set_model_state', SetModelState)
    self.get_model_prox = rospy.ServiceProxy('gazebo/get_model_state', GetModelState)

    
  def spawn_model(self, model_name, model_pose):
    with open("/simulation_ws/src/ros/turtlebot3_gazebo/models/light_green/model.sdf", "r") as f:
      model_xml = f.read()
    orient = Quaternion(*tf.transformations.quaternion_from_euler(model_pose[3], model_pose[4], model_pose[5]))
    item_pose = Pose(Point(x=model_pose[0], y=model_pose[1], z=model_pose[2]), orient)
    print(self.spawn_model_prox(model_name, model_xml, "", item_pose, "world"))

  def get_model_state(self, model_name, rel_model_name='body'):
    return self.get_model_prox(model_name, rel_model_name).pose

  def set_model_state(self, model_name, model_pose):
    model_state = ModelState()
    model_state.model_name = model_name
    model_state.pose.position.x = model_pose[0]
    model_state.pose.position.y = model_pose[1]
    model_state.pose.position.z = model_pose[2]
    model_state.reference_frame = 'world'
    print(self.set_model_prox(model_state))
  
  def delete_model(self, model_name):
    print(self.del_model_prox(model_name))
  
if __name__ == "__main__":
  try:
    ros_service = RosService()
    # ros_service.set_model_state('box_orange', [0,0,0])
    # ros_service.set_model_state('box_black', [1,0,0])
    # pose = ros_service.get_model_state('red_state', rel_model_name='red_state')
    # print(pose)
    ros_service.set_model_state('cylinder_green', [0,0,0])
    ros_service.set_model_state('cylinder_red', [10,0,0])

    # ros_service.spawn_model('test', [0,0,1,0,0,0])
  except rospy.ServiceException, e:
     print "Service call failed: %s"%e