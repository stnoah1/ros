#!/usr/bin/env python
# license removed for brevity
import rospy
from vipo_msgs.msg import RdfMsg
from move_base_seq import MoveBaseSeq


def talker(done):
    pub = rospy.Publisher('rdf_msg', RdfMsg, queue_size=10)
    rospy.init_node('rdf_msg_publisher')
    rate = rospy.Rate(10)
    msg = RdfMsg()
    msg.id = 'robot_01'
    msg.name = 'turtlebot'
    msg.description = ''
    msg.location = '0,0'
    msg.size = ''
    msg.imgUrl = ''
    msg.done = done
    msg.properties[0].name = ''
    msg.properties[0].value = 0
    msg.methods[0].name = 'move'
    msg.methods[0].vipo_msg_type = 'location'
    msg.methods[0].topic_name = 'waypoints'

    while not rospy.is_shutdown():
        rospy.loginfo(msg)
        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        moveBaseSeq = MoveBaseSeq()
        rdf_done = moveBaseSeq.done
        talker(rdf_done)
    except rospy.ROSInterruptException:
        pass
