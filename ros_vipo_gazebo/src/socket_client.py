# modified from the socketIO-client README
# credit: https://github.com/invisibleroads/socketIO-client

from socketIO_client_nexus import SocketIO, LoggingNamespace

# IP_ADDR = '127.0.0.1'
IP_ADDR = 'https://crowd.ecn.purdue.edu/28'

socket = SocketIO(IP_ADDR, None, LoggingNamespace)

def on_connect(data):
    print('[connected]:', data)
    socket.emit('client-connect', 'hello from ros')

def send_rdf(msg):
    socket.emit('rdf_msg', msg)
    socket.wait(seconds=0.5)

socket.on('connected', on_connect)


if __name__ == '__main__':
    def on_vipo_msg(msg):
        print('get script from VIPO', msg)

    socket.on('vipo_msg', on_vipo_msg)
    send_rdf({'deviceId': 1})

    socket.wait()

