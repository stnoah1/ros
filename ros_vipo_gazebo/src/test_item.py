#!/usr/bin/env python

import rospy
import json

from nav_msgs.msg import Odometry
from std_msgs.msg import String

def test_topic(msg=None):
  pub = rospy.Publisher('carry_item', String, queue_size=10)
  if not msg:
    msg = {
        'item':'cylinder',  # box, cylinder
        'type':'disappear',    # appear, disappear
        'color':'orange'       # red, orange, red, blue, black, blue
    }
  rate = rospy.Rate(10)
  while(pub.get_num_connections() == 0):
      rate.sleep()
  pub.publish(str(msg))


if __name__ == '__main__':
  try:
    test_topic()
  except rospy.ROSInterruptException:
    pass
