#!/usr/bin/env python

import rospy
import time
# from vipo_msgs.msg import VipoMsg
from std_msgs.msg import String

TOPIC_INFO ={
    "dev02_dispenseItems": {
        "iot_id":"itemInv01",
        "item_msg": "{'item':'box','type':'appear','color':'blue'}"
    },
    "dev03_dispenseCans": {
        "iot_id":"paintInv01",
        "item_msg": "{'item':'cylinder','type':'appear','color':'orange'}"
    },
    "dev04_dispense": {
        "iot_id":"paintMixer01",
        "item_msg": "{'item':'cylinder','type':'appear','color':'orange'}"
    },
    "dev05_dispense": {
        "iot_id":"paintMixer02",
        "item_msg": "{'item':'cylinder','type':'appear','color':'orange'}"
    },
    "dev06_dispense": {
        "iot_id":"paintMachine01",
        "item_msg": "{'item':'box','type':'appear','color':'orange'}"
    },
    "dev07_dispense": {
        "iot_id":"curingOven01",
        "item_msg": "{'item':'box','type':'appear','color':'orange'}"
    },
}


def carry_item(data, topic_name):
  rate = rospy.Rate(10)
  pub = rospy.Publisher('carry_item', String, queue_size=10)
  msg = TOPIC_INFO[topic_name]['item_msg']
  while(pub.get_num_connections() == 0):
      rate.sleep()
  print('sender', msg)
  pub.publish(str(msg))

def listener():
    for topic_name in TOPIC_INFO.keys():
        rospy.Subscriber(topic_name, String, callback=carry_item, callback_args=topic_name)
    
if __name__ == "__main__":
    rospy.init_node('vipo_connection')
    listener()
    rospy.spin()
