#!/usr/bin/env python

import rospy
from gazebo_msgs.msg import ModelState, ModelStates

from nav_msgs.msg import Odometry
from spawn_model import RosService

GARAGE_POSE = [-10.7265, -6.30681, 0]


def get_robot_location(data):
  global robot_pose_x
  global robot_pose_y
  global robot_flag

  robot_pose_x = data.pose.pose.position.x
  robot_pose_y = data.pose.pose.position.y
  robot_flag = True


def attach_box(item):
  global robot_pose_x
  global robot_pose_y
  global robot_flag
  global detach
  global _item

  _item = item
  robot_flag = False
  detach = False
  
  rospy.Subscriber("odom", Odometry, get_robot_location)

  pub = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=10)
  rate = rospy.Rate(100)  # 10hz
  while not rospy.is_shutdown():
    if robot_flag:
      msg = ModelState()
      msg.pose.position.x = robot_pose_x
      msg.pose.position.y = robot_pose_y
      msg.pose.position.z = 0.8
      msg.model_name = item
      pub.publish(msg)
      if detach:
        ros_service = RosService()
        ros_service.set_model_state(_item, GARAGE_POSE)
        break
      rate.sleep()
  rospy.spin()


def detach_box():
  global detach

  detach = True


if __name__ == '__main__':
  try:
    attach_box()
  except rospy.ROSInterruptException:
    pass
