#!/usr/bin/env python

import rospy
from vipolib import Context
from vipo_msgs.msg import RdfMsg

from socket_client import socket, send_rdf

# global context to save all connected IoTs and robots
context = Context()


def rdf_msg_callback(rdf):
    context.update(rdf)  # update the context with a new rdf
    rdf = context.diff(rdf)  # a dictionary
    if rdf:
        print('Diff: ', rdf)
    # send_rdf(rdf)   # send the rdf to VIPO via socket


def handle_vipo_msg(msg):
    print('Script from VIPO:', msg)
    exec(msg["script"]) in globals(), locals()    # convert the VIPO script into a python class
    Task().run(context)  # pass the global context into task


def main_listener():
    rospy.init_node('master', anonymous=True)
    rospy.Subscriber('rdf_msg', RdfMsg, rdf_msg_callback)

    # option1: use a Subscriber to get msg
    # rospy.Subscriber('vipo_msg', VipoMsg, vipo_msg_callback)
    # option2: use the socket to get the msg directly
    socket.on('vipo_msg', handle_vipo_msg)
    
    print('ros_master is running...')
    while not rospy.core.is_shutdown():
        socket.wait(seconds=0.5)
        rospy.rostime.wallsleep(0.5)


if __name__ == '__main__':
    main_listener()
