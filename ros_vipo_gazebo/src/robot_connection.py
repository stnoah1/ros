#!/usr/bin/env python

import rospy
import json
import ast
from std_msgs.msg import String
from move_box import attach_box, detach_box

def callback(data):
    robot_cmd = ast.literal_eval(data.data)
    print('receiver', robot_cmd)
    print('type', robot_cmd['type'])
    item= '{}_{}'.format(robot_cmd['item'], robot_cmd['color'])
    if robot_cmd['type'] == 'appear':
        print('attach')
        attach_box(item)
    elif robot_cmd['type'] == 'disappear':
        print('detach')
        detach_box()
    else:
        print('type error')

if __name__ == "__main__":
    rospy.init_node('robot_connection')
    rospy.Subscriber("carry_item", String, callback)
    rospy.spin()
