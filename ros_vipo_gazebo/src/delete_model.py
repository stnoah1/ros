#!/usr/bin/env python

import rospy
import tf
from gazebo_msgs.srv import DeleteModel, SpawnModel, SpawnModelRequest
from geometry_msgs.msg import Pose, Point, Quaternion


def delete_model(model_name):
  rospy.init_node("delete_model")
  del_model_prox = rospy.ServiceProxy('gazebo/delete_model', DeleteModel)
  rospy.wait_for_service('gazebo/delete_model')
  print(del_model_prox(str(model_name)))  # Remove from Gazebo

if __name__ == "__main__":
  try:
     delete_model('box_green')
  except rospy.ServiceException, e:
     print "Service call failed: %s"%e