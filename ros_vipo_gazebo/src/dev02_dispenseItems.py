#!/usr/bin/env python

import rospy
import time
from vipo_msgs.msg import VipoMsg

from test_item import test_topic
from test_change_light import change_light

TOPIC_INFO ={
    "iot_id":"itemInv01",
    "item_msg": "{'item':'box','type':'appear','color':'blue'}"
}

def carry_item(data):
    test_topic(TOPIC_INFO['item_msg'])
    change_light({'id':TOPIC_INFO['iot_id'],'state':'on'})
    
if __name__ == "__main__":
    rospy.init_node('dev02_dispenseItems')
    rospy.Subscriber("dev02_dispenseItems", VipoMsg, carry_item)
    rospy.spin()
