#!/usr/bin/env python

import rospy
import json
import ast
import time
from std_msgs.msg import String
from move_box import attach_box, detach_box
from spawn_model import RosService

GARAGE_POSE = [-10.8334, -6.43618, 0.5]
LIGHT_POSE ={
    'curingOven01' : [10.4891, 3.736388, 0.5],
    'paintMachine01' : [3.15121, 1.54111, 0.5],
    'paintMixer01' : [14.1899, -0.668954, 0.5],
    'paintMixer02' : [17.0087, -5.82851, 0.5],
    'charger01' : [-4.015358, -6.0819, 0.5],
    'itemInv01' : [5.141, -7.918, 0.5],
    'paintInv01' : [15.307, -10.0881, 0.5],
}
def callback(data):
    robot_cmd = ast.literal_eval(data.data)
    item = robot_cmd['id']
    ros_service = RosService()
    if robot_cmd['state'] == 'on':
        print('on')
        ros_service.set_model_state('{}_off'.format(item), GARAGE_POSE)
        ros_service.set_model_state('{}_on'.format(item), LIGHT_POSE[item])
    elif robot_cmd['state'] == 'off':
        print('off')
        ros_service.set_model_state('{}_on'.format(item), GARAGE_POSE)
        ros_service.set_model_state('{}_off'.format(item), LIGHT_POSE[item])
    else:
        print('type error')    

if __name__ == "__main__":
    rospy.init_node('iot_connection')
    rospy.Subscriber("change_light", String, callback)
    rospy.spin()
