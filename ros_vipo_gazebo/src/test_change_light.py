#!/usr/bin/env python

import rospy
import json

from nav_msgs.msg import Odometry
from std_msgs.msg import String

def change_light(msg=None):
  pub = rospy.Publisher('change_light', String, queue_size=10)
  if not msg:
    msg = {
        'id':'paintMixer01', # painting_machine, curing_machine, mixer-A, mixer-B, charging_station, itemInv01, paintInv01
        'state':'on'   # on, off
    }
  rate = rospy.Rate(10)
  while(pub.get_num_connections() == 0):
      rate.sleep()
  pub.publish(str(msg))


if __name__ == '__main__':
  try:
    test_topic()
  except rospy.ROSInterruptException:
    pass
