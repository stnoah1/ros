#!/usr/bin/env python

import rospy

from gazebo_msgs.srv import DeleteModel, SpawnModel, GetModelState, SetModelState
from gazebo_msgs.msg import ModelState, ModelStates

from nav_msgs.msg import Odometry

def get_model_location(data):
  global model_data
  global model_flag

  model_data = data
  model_flag = True

def change_light(item, state='off'):
  global model_data
  global model_flag
  model_flag = False

  rospy.init_node('change_color')
  rospy.Subscriber("gazebo/model_states", ModelStates, get_model_location)
  pub = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=10)
  rate = rospy.Rate(100)  # 10hz

  i=0
  while not rospy.is_shutdown():
    if model_flag:
      item_idx = model_data.name.index(item)
      item_pose = model_data.pose[item_idx]
      
      msg = ModelState()
      msg.pose = item_pose
      msg.model_name = 'red_state'
      pub.publish(msg)

      red_idx = model_data.name.index('red_state')
      item_pose = model_data.pose[red_idx]
      
      msg = ModelState()
      msg.pose = item_pose
      msg.model_name = item
      pub.publish(msg)
      i = i + 1
      if i == 10:
        break
      # item_idx = model_data.name.index(item)
      # item_pose = model_data.pose[item_idx]
      # msg = ModelState()
      # msg.pose.position.x = 0
      # msg.pose.position.y = 0
      # msg.pose.position.z = 0
      # msg.model_name = "red_state"
      # pub.publish(msg)
    rate.sleep()
  rospy.spin()


if __name__ == '__main__':
  try:
    change_light('curing_machine_state', state='off')
  except rospy.ROSInterruptException:
    pass
