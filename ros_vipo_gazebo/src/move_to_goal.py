#!/usr/bin/env python

import rospy
import math
import time
import json
# import actionlib
# from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
# from actionlib_msgs.msg import GoalStatus
from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from std_msgs.msg import String
from tf.transformations import euler_from_quaternion
from vipo_msgs.msg import VipoMsg, RdfMsg, Property, Method

PENDING = 0
RUNNING = 1


class MoveToGoal():
    def __init__(self):
        rospy.init_node('move_to_goal', log_level=rospy.WARN)
        self.rate = rospy.Rate(1)

        self.x = 0
        self.y = 0
        self.yaw = 0
        self.linear_speed = 0.0
        self.angular_speed = 0.0

        self.x_goals = []
        self.y_goals = []
        self.yaw_goals = []
        self.goal_cnt = 0

        self.ready_to_drop = False

        self.k_linear = 0.2
        self.k_angular = 1
        self.l_reached = False
        self.rotation_flag = False

        self.status = PENDING
        self.prev_seq = None
        self.done = False
        # self.drop = 'pending'

        rospy.Subscriber('waypoint', VipoMsg, self.move_cb)
        rospy.Subscriber('drop', VipoMsg, self.drop_cb)
        rospy.Subscriber('odom', Odometry, self.position_cb)

    def is_same_msg(self, msg):
        return False
        # is_same = (msg.location == self.prev_seq)
        # self.prev_seq = msg.location
        # return is_same

    def drop_cb(self, msg):
        if self.ready_to_drop:
            print('dropping...')
            self.drop_publisher()
            self.done = True
            self.rdf_publisher()
            self.done = False
            self.rdf_publisher()
            self.ready_to_drop = False

    def move_cb(self, msg):
        if self.status == PENDING:
            if self.is_same_msg(msg):
                return
            else:
                self.status = RUNNING

        goals = list(msg.location)
        x_g = []
        y_g = []
        yaw_g = []
        for i in range(0, int(len(goals)/3)):
            x_g.append(goals[3*i])
            y_g.append(goals[3*i+1])
            yaw_g.append(goals[3*i+2])

        if any(x + 12.4 < 0.1 for x in x_g) and any(y + 3.4 < 0.1 for y in y_g):
            x_g.insert(0, -5.21089)
            y_g.insert(0, -4.8842)
            yaw_g.insert(0, -3.4)

        self.x_goals = x_g
        self.y_goals = y_g
        self.yaw_goals = yaw_g
        # print(self.x_goals, self.y_goals, self.yaw_goals)

        self.move_to_goal()

    def position_cb(self, msg):
        self.x = msg.pose.pose.position.x
        self.y = msg.pose.pose.position.y
        quaternion = [msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w]
        euler = euler_from_quaternion(quaternion)
        self.yaw = euler[2]

    def move_to_goal(self):
        if self.status == PENDING:
            return

        if self.goal_cnt < len(self.x_goals):
            x_goal = self.x_goals[self.goal_cnt]
            y_goal = self.y_goals[self.goal_cnt]
            yaw_goal = self.yaw_goals[self.goal_cnt]
            # print(x_goal, y_goal, yaw_goal)

            self.set_vel(x_goal, y_goal, yaw_goal)
            # self.vel_publisher()

        else:
            print('Move command has been done! Updated rdf msg has been published to rdf_msg.')
            self.ready_to_drop = True  # only after move down, we allow drop
            print('ready to drop')
            self.done = True
            self.rdf_publisher()
            self.status = PENDING
            self.goal_cnt = 0
            self.done = False
            self.rdf_publisher()
            self.linear_speed = 0.0
            self.angular_speed = 0.0
            return

    def set_vel(self, x_goal, y_goal, yaw_goal):
        distance_err = math.sqrt((self.x - x_goal) ** 2 + (self.y - y_goal) ** 2)
        yaw_err = math.atan2(y_goal - self.y, x_goal - self.x) - self.yaw
        
        if not self.rotation_flag:
            self.set_rotation(x_goal, y_goal, yaw_goal)
        # while abs(yaw_err) > 0.7 and not self.l_reached:
        #     self.linear_speed = 0.0
        #     self.angular_speed = 0.5
        #     self.vel_publisher()
        #     yaw_err = math.atan2(y_goal - self.y, x_goal - self.x) - self.yaw
        #     if yaw_err < 0.3:
        #         break
        if self.rotation_flag:
            self.linear_speed = 0.7
            self.angular_speed = self.k_angular * yaw_err
            self.vel_publisher()

        # if distance_err < 0.2:
        #     self.linear_speed = 0.0
        #     self.l_reached = True
        #     self.angular_speed = 0.5
        #     if abs(self.yaw - yaw_goal) < 0.5:
        #         self.linear_speed = 0.0
        #         self.angular_speed = 0.0
        #         print('Goal ' + str(self.goal_cnt + 1) + ' arrived.')
        #         self.goal_cnt += 1
        #         self.l_reached = False

        if distance_err < 0.2:
            self.linear_speed = 0.0
            self.angular_speed = 0.0
            self.vel_publisher()
            self.goal_cnt += 1
            self.l_reached = False
            self.rotation_flag = False
            return

    def set_rotation(self, x_goal, y_goal, yaw_goal):
        self.linear_speed = 0.0
        self.angular_speed = 0.5
        self.vel_publisher()
        yaw_err = math.atan2(y_goal - self.y, x_goal - self.x) - self.yaw
        if abs(yaw_err) < 0.5:
            self.angular_speed = 0.0
            self.vel_publisher()
            self.rotation_flag = True

    def vel_publisher(self):
        vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
        velocity_msg = Twist()
        velocity_msg.linear.x = self.linear_speed
        velocity_msg.angular.z = self.angular_speed
        vel_pub.publish(velocity_msg)

    def rdf_publisher(self):
        rdf_pub = rospy.Publisher('rdf_msg', RdfMsg, queue_size=10)
        rdf_msg = RdfMsg()
        rdf_msg.id = 'robot01'
        rdf_msg.name = 'turtlebot'
        rdf_msg.description = ''
        rdf_msg.location = [self.x, self.y]
        rdf_msg.size = ''
        rdf_msg.imgUrl = ''
        rdf_msg.done = self.done
        prop = Property()
        prop.name = ''
        prop.value = 0.0
        rdf_msg.properties.append(prop)
        meth1 = Method()
        meth1.name = 'move'
        meth1.vipo_msg_type = 'location'
        meth1.topic_name = 'waypoint'
        meth2 = Method()
        meth2.name = 'drop'
        meth2.vipo_msg_type = 'object'
        meth2.topic_name = 'drop'
        rdf_msg.methods.append(meth1)
        rdf_msg.methods.append(meth2)
        rdf_pub.publish(rdf_msg)

    def drop_publisher(self):
        drop_pub = rospy.Publisher('carry_item', String, queue_size=10)
        while(drop_pub.get_num_connections() == 0):
            self.rate.sleep()
        drop_msg = {
            'item': '',  # box, cylinder
            'type': 'disappear',  # appear, disappear
            'color': ''  # red, orange, red, blue, black, blue
        }
        print('disappear')
        drop_pub.publish(str(drop_msg))

    def run(self):
        while not rospy.is_shutdown():
            self.rdf_publisher()
            # self.drop_publisher()
            self.rate.sleep()


if __name__ == '__main__':
    try:
        # MoveBaseSeq()
        moveToGoal = MoveToGoal()
        moveToGoal.run()
    except rospy.ROSInterruptException:
        rospy.loginfo("Node terminated.")
