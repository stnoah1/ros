import rospy
import time
from vipo_msgs.msg import VipoMsg

METHOD_KEYS = ['name', 'vipo_msg_type', 'topic_name']
PROPERTY_KEYS = ['name', 'value']

def convert_rdf_to_dict(rdf):
    # print({attr: getattr(rdf, attr) for attr in rdf.__slots__})
    return {attr: getattr(rdf, attr) for attr in rdf.__slots__}

class Context(object):
    """Keep track of all connected IoTs and robots in the format of RDF"""
    def __init__(self):
        self.ctx = {}  # use a dict to store the id and rdf class
        
    def update(self, rdf):
        device_id = rdf.id
        if device_id in self.ctx:
            device = self.ctx[device_id]
            device.update_rdf(rdf)
        else:
            device = RdfDevice(rdf)
            self.ctx[device_id] = device

    def diff(self, new_rdf):
        "Return the difference between new_rdf and old one as a dictionary"
        device = self.get(new_rdf.id)
        return device.diff(new_rdf)

    def get(self, device_id):
        return self.ctx.get(device_id, None)


class RdfDevice(object):
    """Convert a RDF msg into a class
    We overload each func in rdf.methods as `self.func()`
    
    e.g.,
    A RDF msg may look like below
    rdf = {
        "id": "robot_123",
        "properties": "jobStatus,0.8;temperature,400",
        "methods": "move,Location,topic_name;cure,Time,topic_name",
        "done": false
    }
    After convert:
    robot = RdfDevice(rdf)
    robot.move(args)
    """
    def __init__(self, rdf):
        # a dict to store the topic_name and initiated publisher
        self.topic_pub_dict = {}
        self.old_rdf = None
        # set method only once, as rdf.methods usally do not change
        self._set_method_as_attr(rdf)
        self.update_rdf(rdf)
 
    def update_rdf(self, rdf):
        self.id = rdf.id
        self.location = rdf.location
        self.done = rdf.done
        self._set_property_as_attr(rdf)

    def diff(self, new_rdf):
        if not self.old_rdf:
            self.old_rdf = new_rdf
            return convert_rdf_to_dict(new_rdf)
        df = {}
        for key, value in convert_rdf_to_dict(new_rdf).items():
            if value != getattr(self.old_rdf, key):
                df[key] = value
        self.old_rdf = new_rdf
        return df

    def _set_method_as_attr(self, rdf):
        for func, _type, topic_name in self._parse_attrs(rdf.methods, METHOD_KEYS):
            setattr(self, func, self._convert(topic_name))

    def _set_property_as_attr(self, rdf):
        for prop, value in self._parse_attrs(rdf.properties, PROPERTY_KEYS):
            setattr(self, prop, value)

    def _parse_attrs(self, attrs, keys):
        if not attrs:
            return []
        if isinstance(attrs, str):
            return [attr.split(',') for attr in attrs.split(';')]
        elif isinstance(attrs, list):
            return [[getattr(attr, key) for key in keys] for attr in attrs]
        else:
            raise TypeError('invalid attrs', type(attrs), attrs)


    def _get_publisher(self, topic_name):
        "Get publisher by topic_name or create new one and save to cache"
        if topic_name in self.topic_pub_dict:
            return self.topic_pub_dict[topic_name]
        pub = rospy.Publisher(topic_name, VipoMsg, queue_size=10)
        self.topic_pub_dict[topic_name] = pub
        return pub

    def _convert(self, topic_name):
        max_wait_cycle = 100
        def inner_func(msg):
            pub = self._get_publisher(topic_name)
            vipomsg = VipoMsg()
            for key in msg:
                setattr(vipomsg, key, msg[key])
            print(vipomsg)
            
            # wait for one command to finish before starting the next command
            # self.done will be updated in self.update_rdf(rdf)
            # which is called by context.update(rdf)
            wait_cycle = 0
            while not self.done:
                print('wait cycle', wait_cycle)
                pub.publish(vipomsg)
                wait_cycle += 1
                if wait_cycle > max_wait_cycle:
                    break
                time.sleep(1)
            print('done')

        return inner_func

# class RdfDeviceV2(object):
#     """Convert a RDF msg into a class
#     We overload each method/property in rdf.methods as `self.func()`
    
#     e.g.,
#     A RDF msg may look like below
#     rdf = {
#         "id": "robot_123",
#         "methods": [{"move": "topic_name"}],
#         "properties": [{"jobStatus": 0.8}, {"temperature": 400}],
#         "done": false
#     }
#     After convert:
#     robot = RdfDevice(rdf)
#     robot.move(args)
#     """
#     def __init__(self, rdf):
#         self.topic_pub_dict = {}
#         self.update_rdf(rdf)

#     def update_rdf(self, rdf):
#         self.id = rdf.id
#         self.location = rdf.location
#         self.done = rdf.done
#         for prop in rdf.properties:
#             setattr(self, prop, rdf.properties[prop])
#         for func in self.methods:
#             setattr(self, func, self.__parse(rdf.methods[func]))

#     def get_publisher(self, topic_name):
#         if topic_name in self.topic_pub_dict:
#             return self.topic_pub_dict[topic_name]
#         pub = rospy.Publisher(topic_name, VipoMsg, queue_size=10)
#         self.topic_pub_dict[topic_name] = pub
#         return pub
        
#     def __parse(self, func_detail):
#         topic_name, _ = func_detail.split(',')
#         max_wait_cycle = 10
#         def inner_func(msg):
#             # wait for one command to finish before starting the next command
#             pub = self.get_publisher(topic_name)
#             pub.publish(msg)
#             wait_cycle = 0
#             while not self.done:
#                 print('wait cycle', wait_cycle)
#                 wait_cycle += 1
#                 if wait_cycle > max_wait_cycle:
#                     break
#                 time.sleep(1)

#         return inner_func


# Abandon
# class RdfDeviceV1(object):
#     """Convert a RDF msg into a class
#     We use self.do(func) to call each func in rdf.funcs
    
#     e.g.,
#     A RDF msg may look like below
#     rdf = {"id": "robot_123", "funcs": {"move": "details here"}}
#     After convert:
#     robot = RdfDevice(rdf)
#     robot.do("move", args)
#     """
#     def __init__(self, rdf):
#         self.id = rdf.id
#         self.funcs = rdf.funcs

#     def update(self, rdf):
#         self.funcs = rdf.funcs
    
#     def do(self, func):
#         # the detail of each func of the device is saved in self.funcs
#         # NOTE: the exact way to exec the detail is TBD
#         exec(self.funcs[func], args)
